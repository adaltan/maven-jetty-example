-- 2012/09/06/Thu

org.mortbay.jetty/jetty-maven-plugin에서 기존에는 jetty 기동중일때, 정적 파일을 수정하면, 뭔가 블록되서 골치가 아팠었는데 이제는 잘되는듯?
-- 아닌듯. 여전히 index.jsp등이 아니라 참조하는 파일(index.js...)등의 파일은 똑같이 문제가 있군 -,.-
	-- oejs.Server:jetty-8.1.3.v20120416
	
윈도에서는 공통적으로 문제가 되는듯.
http://docs.codehaus.org/display/JETTY/Files+locked+on+Windows

	<web-app ...>
	...
	<servlet>
	    <servlet-name>default</servlet-name>
	    <servlet-class>org.mortbay.jetty.servlet.DefaultServlet</servlet-class>
	    <init-param>
	      <param-name>useFileMappedBuffer</param-name>
	      <param-value>false</param-value>
	    </init-param>
	    <load-on-startup>0</load-on-startup>
	  </servlet>
	...
	</web-app>


## 해결책!
	<servlet>
		<!-- Override init parameter to avoid nasty -->
		<!-- file locking issue on windows. -->
		<servlet-name>default</servlet-name>
		<init-param>
			<param-name>useFileMappedBuffer</param-name>
			<param-value>false</param-value>
		</init-param>
	</servlet>
	
	위의 내용을 그냥 web.xml에 집어 넣는다. webdefault.xml에서 이미 정의했겠지만, 이를 엎어치는 방식인듯.


## <scanIntervalSeconds>10</scanIntervalSeconds>
	-- 요것도 관련된 영향은 없는듯.
	

## 더 나은 해결책?
	
	일단 web.xml에 서블릿 설정 다시 지정하고 그러는건 조금 거시기한듯하고. 웹검색으로 나오는 webdefault.xml을 엎어쳐라 같은건 더 거시기하고;;;
	
	그래서 아예 소스를 까보기로.
	
		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-servlet</artifactId>
			<version>8.1.5.v20120716</version>
		</dependency>
		
	요기에서 org.eclipse.jetty.servlet.DefaultServlet을 찾았고, 요놈이 정적 파일을 서빙하는걸로.
	
	getServletContext().getInitParameter("org.eclipse.jetty.servlet.Default."+name);
		name은 useFileMappedBuffer정도겠으니...
	
	이렇게 찾아내서 결국 web.xml에 다음을 설정하면 되는걸 알았심..
	
	<context-param>
		<param-name>org.eclipse.jetty.servlet.Default.useFileMappedBuffer</param-name>
		<param-value>false</param-value>
	</context-param>
	
	ㅎㅎ 베리 잘됨.
	
	더 과거 버전의 Jetty일때는, eclipse-jetty이 아니라, mortbay-jetty 버젼일때는, 키 이름을 바꿔서.
	
	<param-name>org.eclipse.jetty.servlet.Default.useFileMappedBuffer</param-name>
	...대신에 다음으로 설정하면됨...
	<param-name>org.mortbay.jetty.servlet.Default.useFileMappedBuffer</param-name>



### 끝